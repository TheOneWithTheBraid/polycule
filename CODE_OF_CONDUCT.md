# Code of conduct

< polycule > orients with its code of conduct to the \[matrix\] foundation's code of conduct.

As the \[matrix\] community, we strive to :

- Be friendly and patient.
- Be welcoming.
- Be considerate.
- Be respectful.
- Be careful in the words that we choose.
- Try to understand why we disagree.

In order to get a deeper idea of how this is to be understood,
consult [the \[matrix\] code of conduct](https://matrix.org/legal/code-of-conduct/).

🏳️‍🌈 rights are human rights.
