## This year in < polycule >

### 2024 in < polycule >

Welcome to < polycule > - a brand-new, geeky and next-gen \[matrix\] client.

< polycule > initially started as personal \[matrix\]
playground [on January 16th 2024](https://gitlab.com/polycule_client/polycule/-/commit/08294e02) based on
the [Matrix Dart SDK](https://github.com/famedly/matrix-dart-sdk/). While it shares the SDK and many best practices with
FluffyChat, < polycule > is no fork of Krille's amazing work but completely written from scratch.

My initial aim was to play around with some Flutter Linux native integration, efficient handling of the Matrix Dart
SDK's sync responses and performant rendering. After some initial experiments, < polycule > turned out to become a more
and more serious project.

So far, it became
the [Dart's initial Sliding Sync implementation](https://slides.com/theonewiththebraid/sliding-sync/), [native OIDC ready](https://areweoidcyet.com/),
provides [advanced accessibility features](https://alpaka.garden/@braid/113333837407822399) and keyboard support on
Linux.

< polycule > in general aims to bring a geeky, terminal-like \[matrix\] user experience on Linux
desktops, [Linux mobile](https://alpaka.garden/@braid/113276065629145919) and Android.

If you are curious about the current feature support of < polycule >, you can consult
the [roadmap on GitLab](https://gitlab.com/polycule_client/polycule#roadmap).

12 months and 209 merge requests after the initial commit, the project so far is available as vividly developed alpha
version available on Alpine Linux, postmarket OS, the AUR as well as an APK for Android.

Read all : https://blog.neko.dev/posts/matrix-year-in-review-2024.html
