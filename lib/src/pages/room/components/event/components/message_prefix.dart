import 'package:flutter/material.dart';

import 'package:matrix/matrix.dart';

import '../../../../../../l10n/generated/app_localizations.dart';
import '../../../../../router/extensions/go_router_path_extension.dart';
import '../../../../../utils/matrix/neighboaring_event_extension.dart';
import '../../../../../utils/matrix/same_message_bubble_extension.dart';
import '../../../../../widgets/matrix/scopes/event_scope.dart';
import '../../../../../widgets/matrix/scopes/timeline_scope.dart';
import '../../../../user_page/user_page.dart';
import '../../message_user_avatar.dart';
import 'edit_tooltip.dart';

class MessagePrefix extends StatelessWidget {
  const MessagePrefix({super.key});

  @override
  Widget build(BuildContext context) {
    final timeline = TimelineScope.of(context).timeline;
    final event = EventScope.of(context).event.getDisplayEvent(timeline);
    timeline.getPreviousDisplayEvent(timeline.events.indexOf(event));
    final nextEvent =
        timeline.getNextDisplayEvent(timeline.events.indexOf(event));

    final isOwnMessage = event.senderId == event.room.client.userID;

    final edits =
        event.aggregatedEvents(timeline, RelationshipTypes.edit).toList();
    edits.sort(
      (a, b) => a.originServerTs.compareTo(b.originServerTs),
    );
    final editEvent = edits.lastOrNull;

    final nextMessageSameSender =
        nextEvent?.isSameMessageBubble(event) ?? false;
    final showOtherSenderAvatar = !isOwnMessage && !nextMessageSameSender;

    Widget? prefix;

    Widget? editNotice;

    if (event.redacted) {
      editNotice = const Icon(Icons.delete);
    } else if (editEvent != null) {
      editNotice = EditTooltip(editEvent: editEvent);
    }

    if (showOtherSenderAvatar) {
      prefix = MessageUserAvatar(
        event: event,
        onTap: () => context.pushMultiClient(
          UserPage.makeRouteName(event.senderId),
        ),
      );
      if (event.messageType == MessageTypes.Notice) {
        prefix = Stack(
          alignment: Alignment.bottomRight,
          children: [
            prefix,
            const Padding(
              padding: EdgeInsets.all(2.0),
              child: Icon(Icons.smart_toy),
            ),
          ],
        );
      }
    } else if (event.status.isError) {
      prefix = IconButton(
        tooltip: AppLocalizations.of(context).retrySending,
        onPressed: event.sendAgain,
        icon: const Icon(Icons.restart_alt),
      );
    } else if (event.status.isSending) {
      prefix = IconButton(
        tooltip: AppLocalizations.of(context).cancelSending,
        onPressed: event.cancelSend,
        icon: const Icon(Icons.cancel_rounded),
      );
    } else if (isOwnMessage) {
      prefix = editNotice;
    } else {
      prefix = null;
    }

    return IconTheme(
      data: IconTheme.of(context).copyWith(size: 16),
      child: SelectionArea(
        child: SizedBox.square(
          dimension: 32,
          child: prefix,
        ),
      ),
    );
  }
}
