import 'package:flutter/material.dart';

import 'homeserver_view.dart';

class HomeserverPage extends StatelessWidget {
  const HomeserverPage({super.key});

  static const routeName = '/login';

  @override
  Widget build(BuildContext context) => const HomeserverView();
}
