import 'package:flutter/material.dart';

import '../../../../l10n/generated/app_localizations.dart';

class ShareText extends StatelessWidget {
  const ShareText({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(AppLocalizations.of(context).sharingText);
  }
}
