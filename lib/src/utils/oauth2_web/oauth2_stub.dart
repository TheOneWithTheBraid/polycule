Stream<Uri> listenWebBroadcastChannel() =>
    const Stream<Uri>.empty(broadcast: true);
