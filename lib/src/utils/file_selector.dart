import 'dart:convert';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:file_selector/file_selector.dart';
import 'package:image_picker/image_picker.dart';
import 'package:matrix/matrix.dart';
import 'package:media_kit/media_kit.dart';
import 'package:media_store_plus/media_store_plus.dart';

import '../../l10n/generated/app_localizations.dart';
import '../widgets/file_preview_dialog/file_preview_dialog.dart';
import '../widgets/matrix/scopes/matrix_scope.dart';

class FileSelector {
  FileSelector(this.msgType);

  static bool _mediaStoreInitialized = false;

  bool compress = false;
  List<XFile>? files;

  final String? msgType;

  bool get useImagePicker =>
      !kIsWeb &&
      (Platform.isAndroid || Platform.isIOS) &&
      const [MessageTypes.Image, MessageTypes.Video].contains(msgType);

  static Future<void> ensureAndroidInitialized() async {
    if (!_mediaStoreInitialized) {
      await MediaStore.ensureInitialized();
      MediaStore.appFolder = 'polycule';
      _mediaStoreInitialized = true;
    }
  }

  List<XTypeGroup> _createTypeTypeGroups(AppLocalizations l10n) {
    final useUTI = kIsWeb ? false : Platform.isIOS || Platform.isMacOS;

    final xTypeGroups = <XTypeGroup>[
      XTypeGroup(
        label: l10n.typeGroupFiles,
        mimeTypes: const [],
        uniformTypeIdentifiers: useUTI ? const ['public.content'] : null,
      ),
    ];

    switch (msgType) {
      case MessageTypes.Image:
        xTypeGroups.insertAll(0, [
          XTypeGroup(
            label: l10n.typeGroupImages,
            mimeTypes: const [
              'image/*',
              'application/json',
            ],
            extensions: [
              // Lottie files
              '.lottie',
              '.tgs',
            ],
            uniformTypeIdentifiers: const ['public.image'],
          ),
        ]);
        break;
      case MessageTypes.Video:
        xTypeGroups.insertAll(0, [
          XTypeGroup(
            label: l10n.typeGroupVideos,
            mimeTypes: const ['video/*'],
            uniformTypeIdentifiers: const ['public.video'],
          ),
        ]);
        break;
      case MessageTypes.Audio:
        xTypeGroups.insertAll(0, [
          XTypeGroup(
            label: l10n.typeGroupAudio,
            mimeTypes: const ['audio/*'],
            uniformTypeIdentifiers: const ['public.audio'],
          ),
        ]);
        break;
    }

    return xTypeGroups;
  }

  Future<bool> selectFiles(
    BuildContext context, {
    bool enforceSingle = false,
  }) async {
    final l10n = AppLocalizations.of(context);
    List<XFile> files;

    if (useImagePicker &&
        !kIsWeb &&
        (Platform.isAndroid || Platform.isIOS || Platform.isMacOS)) {
      final picker = ImagePicker();
      if (msgType == MessageTypes.Image) {
        if (enforceSingle) {
          final file = await picker.pickImage(source: ImageSource.gallery);
          if (file == null) {
            return false;
          }
          files = this.files = [file];
        } else {
          files = this.files = await picker.pickMultiImage();
        }
      } else {
        final file = await picker.pickVideo(source: ImageSource.gallery);
        if (file == null) {
          return false;
        }
        files = this.files = [file];
      }
    } else {
      if (enforceSingle) {
        final file = await openFile(
          acceptedTypeGroups: _createTypeTypeGroups(l10n),
        );
        if (file == null) {
          return false;
        }
        files = this.files = [file];
      } else {
        files = this.files = await openFiles(
          acceptedTypeGroups: _createTypeTypeGroups(l10n),
        );
      }
    }
    return files.isNotEmpty;
  }

  Future<FileSendProperties?> previewSelection(
    BuildContext context, {
    bool allowCompress = true,
  }) async {
    final files = this.files;
    if (files == null || files.isEmpty) {
      return null;
    }
    final scope = MatrixScope.captureAll(context);
    final selection = await showAdaptiveDialog<FileSendProperties>(
      context: context,
      builder: (context) => MatrixScope(
        scope: scope,
        child: FilePreviewDialog(
          files: files,
          allowCompress: allowCompress,
        ),
      ),
      useRootNavigator: true,
    );
    if (selection == null) {
      return null;
    }

    compress = selection.compress;
    this.files = selection.files;
    return selection;
  }

  Future<List<MatrixFileTuple>> makeMatrixFiles(
    BuildContext context,
    NativeImplementations nativeImplementations,
  ) async {
    final files = this.files;

    if (files == null || files.isEmpty) {
      return [];
    }

    List<MatrixFileTuple> matrixFiles = [];

    for (var file in files) {
      final bytes = await file.readAsBytes();
      int? width, height;

      String? mimeType = file.mimeType;
      // the SDK keeps thinking webm files are audio
      if (mimeType == null && file.name.endsWith('.webm')) {
        mimeType = 'video/webm';
      }

      bool enforceImage = false;
      // check for Telegram sticker
      if (file.name.endsWith('.tgs')) {
        enforceImage = true;
        mimeType = 'application/gzip';
      }
      // check for dotLottie
      if (file.name.endsWith('.lottie')) {
        enforceImage = true;
        mimeType = 'application/zip';
      }
      // check for Lottie file
      if (mimeType == 'application/json' || file.name.endsWith('.json')) {
        try {
          final json = jsonDecode(utf8.decode(bytes));
          if (json is Map &&
              json.containsKey('layers') &&
              json.containsKey('nm')) {
            enforceImage = true;
            mimeType = 'application/json';
            width = json['w'] as int?;
            height = json['h'] as int?;
          }
        } catch (_) {
          // obviously not a Lottie files
        }
      }
      MatrixFile matrixFile;
      if (enforceImage) {
        matrixFile = MatrixImageFile(
          bytes: bytes,
          name: file.name,
          mimeType: mimeType,
          width: width,
          height: height,
        );
      } else {
        matrixFile = MatrixFile.fromMimeType(
          bytes: bytes,
          name: file.name,
          mimeType: mimeType,
        );
      }
      if (matrixFile is MatrixImageFile) {
        if (compress) {
          matrixFile = await MatrixImageFile.shrink(
            bytes: matrixFile.bytes,
            name: matrixFile.name,
            maxDimension: 2160,
            mimeType: matrixFile.mimeType,
            nativeImplementations: nativeImplementations,
          );
        }

        final tuple = MatrixFileTuple(
          file: matrixFile,
          // the thumbnail is generated in the SDK
        );
        matrixFiles.add(tuple);
      } else if (matrixFile is MatrixAudioFile) {
        matrixFiles.add(MatrixFileTuple(file: matrixFile));
      } else if (matrixFile is MatrixVideoFile) {
        MatrixImageFile? thumbnail;
        try {
          final player = Player();
          final playable = await Media.memory(matrixFile.bytes);
          player.open(playable);
          const mime = 'image/png';
          final thumbnailData = await player.screenshot(format: mime);
          if (thumbnailData != null) {
            thumbnail = await MatrixImageFile.shrink(
              bytes: thumbnailData,
              name: 'thumbnail.png',
              mimeType: mime,
              nativeImplementations: nativeImplementations,
            );
          }
        } catch (e, s) {
          Logs().d('Error creating video thumbnail', e, s);
        }
        matrixFiles.add(
          MatrixFileTuple(
            file: matrixFile,
            thumbnail: thumbnail,
          ),
        );
      } else {
        matrixFiles.add(MatrixFileTuple(file: matrixFile));
      }
    }

    return matrixFiles;
  }
}

class MatrixFileTuple {
  const MatrixFileTuple({required this.file, this.thumbnail});

  final MatrixFile file;
  final MatrixImageFile? thumbnail;
}
