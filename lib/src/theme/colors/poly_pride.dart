import 'dart:ui';

abstract class PolyColors {
  const PolyColors._();

  static const cyan = Color(0xff00c2ff);
  static const pink = Color(0xffff006a);
  static const grey = Color(0xff0a1027);
  static const white = Color(0xffffffff);

  static const sequence = [
    cyan,
    pink,
    grey,
    white,
  ];
}
